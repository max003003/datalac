<?php
	$objConnect = mysql_connect("127.0.0.1:3306","root","") or die("Error Connect to Database");
	$objDB = mysql_select_db("chatty");
?>



<!DOCTYPE html>
<html>
<head>
	<title>Requisition</title>
</head>
<meta charset="UTF-8" >
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<style type="text/css">
	#table01{
		margin: auto;
	}
	table,tr,td,th{
	    border: 3px solid black;
	    border-collapse: collapse;
		height: 100%;
	}

	#p01{
		text-align: center;
		color: #FFFFFF;
		background-color: #000099;
		padding: 30px;
		font-size: 40px;
	}
	#td01{
		vertical-align: top;
		padding: 5px;
		padding-top: 10px;
		padding-left: 20px;
		padding-right: 10px;
		background-color: #6666ff;
		width: 140px;
		height: 100%;
	}
	#td02{
		vertical-align: top;
		padding: 5px;
		background-color: #b3b3ff;
		width: 550px;
		height: 100%;
	}
	#td01:hover{
		background-color: #8080ff;
		color: blue;
	}
	#td02:hover{
		background-color: #e5e5ff;
		color: blue;
	}
	#table02 tr:nth-child(even) {
    	background-color: #eee;
	}
	#table02 tr:nth-child(odd) {
		background-color:#fff;
	}
	#table02{
		margin: auto;
		width: 95%;
	}
	a{
		color: #000000;
	}
	a:hover{
		color: #FFFFFF;
		font-weight: bolder;
	}
	#a01:hover{
		color: blue;
		font-weight: #FFFFFF;
	}
	#th01,#th02,#th03{
		padding: 8px;
		background-color: #808080;
		color: #FFFFFF;
		text-align: center;
	}
	#th01{
		width: 60px;
	}
	#th03{
		width: 100px;
	}
	
</style>
<body>
	<p id="p01">ระบบเบิกของ</p>
	<br>
	<div id="div00">
		<table id="table01">
			<tr>
				<td id="td01">
					<div id="div01">
						<p><a href="HomePage.php">รายการของ</a></p>
						<p><a href="page02.php">ตรวจสอบการเบิกของ</a></p>
					</div>
				</td>
				<td  id="td02">
					<div id="div02"><br>
						<table id="table02">
							<tr>
								<th id="th01">ลำดับ</th>
								<th id="th02">รายชื่อ</th>
								<th id="th03">รายการที่เบิก</th>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td align="center"><a id="a01" href="page03.php">แสดง</a></td><!-- กดเข้าไปดูรายการ-->
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</div>

	
</body>
</html>
