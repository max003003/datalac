<!DOCTYPE html>
<html>
<head>
	<title>Requisition</title>
</head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<meta charset="UTF-8" >
<style type="text/css">
	#p01{
		text-align: center;
		color: #FFFFFF;
		background-color: #000099;
		padding: 30px;
		font-size: 40px;
	}

	#table01{
		margin: auto;
	}
	table,tr,td,th{
	    border: 3px solid black;
	    border-collapse: collapse;
		height: 100%;
	}
	#td01{
		vertical-align: top;
		padding: 5px;
		padding-top: 10px;
		padding-left: 20px;
		padding-right: 10px;
		background-color: #6666ff;
		width: 140px;
		height: 100%;
	}
	#td02{
		vertical-align: top;
		padding: 5px;
		background-color: #b3b3ff;
		width: 550px;
		height: 100%;
	}
	#td01:hover{
		background-color: #8080ff;
		color: blue;
	}
	#td02:hover{
		background-color: #e5e5ff;
		color: blue;
	}
	table#table02 tr:nth-child(even) {
    	background-color: #eee;
	}
	table#table02 tr:nth-child(odd) {
	   background-color:#fff;
	}
	#table02{
		margin: auto;
		width: 95%;
	}
	a{
		color: #000000;
	}
	a:hover{
		color: #FFFFFF;
		font-weight: bolder;
	}
	#th01,#th02,#th03{
		padding: 8px;
		background-color: #808080;
		color: #FFFFFF;
		text-align: center;
	}
	#th01,#th03{
		width: 60px;
	}
	#sel01{
		width: 100%;
	}
	#div03{
		text-align: right;
	}
	
</style>
<body>
	<p id="p01">ระบบเบิกของ</p>
	<br>
	<div id="div00">
		<table id="table01">
			<tr>
				<td id="td01">
					<div id="div01">
						<p><a href="HomePage.php">รายการของ</a></p>
						<p><a href="page02.php">ตรวจสอบการเบิกของ</a></p>
					</div>
				</td>
				<td id="td02">
					<div id="div02">
						<div id="div03"><a href="loginpage/login.php" class="btn btn-danger" title="ทำรายการเบิกของ">ทำรายการ</a></div>
						<br>
						<table id="table02">
							<tr>
								<th id="th01">ลำดับ</th>
								<th id="th02">รายการ</th>
								<th id="th03">จำนวน</th>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</table>	
					</div>
				</td>
			</tr>
		</table>
	</div>

	
</body>
</html>