<?php
session_start();
if ($_SESSION['sess_userid']<>session_id())
{
	header("Location:admin.php");
	exit();
	}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Requisition</title>
</head>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
	#p01{
		text-align: center;
		color: #000000;
		background-color: #ff9999;
		padding: 30px;
		font-size: 40px;
	}

	#table01{
		margin: auto;
	}
	table,tr,td,th{
	    border: 3px solid black;
	    border-collapse: collapse;
		height: 100%;
	}
	#td01{
		vertical-align: top;
		padding: 5px;
		padding-left: 20px;
		padding-right: 10px;
		background-color: #6666ff;
		width: 140px;
		height: 100%;
	}
	#td02{
		padding: 5px;
		background-color: #ffe5e5;
		width: 550px;
		height: 100%;
	}
	#td01:hover{
		background-color: #8080ff;
		color: blue;
	}
	#td02:hover{
		background-color: #ffb3b3;
		color: blue;
	}
	table#table02 tr:nth-child(even) {
    	background-color: #eee;
	}
	table#table02 tr:nth-child(odd) {
	   background-color:#fff;
	}
	#table02{
		margin: auto;
		width: 95%;
	}
	a{
		color: #000000;
	}
	a:hover{
		color: #FFFFFF;
		font-weight: bolder;
	}
	#th01,#th02,#th03{
		padding: 8px;
		background-color: #808080;
		color: #FFFFFF;
		text-align: center;
	}
	#th01,#th03{
		width: 60px;
	}
	#sel01{
		width: 100%;
	}
	#div001{
		text-align: right;
		padding-right: 10px;
	}
	#a001{
		padding: 3px;
		font-size: 10px;
	}
	#div02{
		border-radius: 15px;
		padding: 50px;
		background-color: #FFFFFF; 
	}
	#div02:hover{
		font-weight: bolder;
	}
	#pDate{
		font-size: 10px;
		color: #FFFFFF;
		vertical-align: bottom;
	}	
</style>
<body>
	<p id="p01">ระบบเบิกของ</p>
	<br>
	<div id="div00">
		<table id="table01">
			<tr>
				<td id="td01">
					<div id="div01">
						<br>
						<p><img id="imAd" src="administrator/images/LogoAdmin.jpg" width="100px" height="130px"></p>
						<p><div id="div001"><a id="a001" href="logout.php" class="btn btn-danger" title="ออกจากระบบ">Logout</a></div></p>
						<p><li><a href="administrator/list/checkList.php">รายการของ</a></li></p>
						<p><li><a href="administrator/list/confirm.php">รายชื่อผู้ทำเรื่องเบิกของ</a></li></p>
						<p id="pDate">
								<span id="date_time"></span>
								<script type="text/javascript" src="js/dateRealtime.js"></script>
            					<script type="text/javascript"> window.onload = date_time('date_time');</script>
            			</p>
					</div>
				</td>
				<td id="td02">
					<div id="div02">
						คุณสามารถทำการจัดการรายการ ตรวจสอบ และอนุมัติผู้ทำเรื่องขอเบิกได้ตามรายการท้างด้านซ้าย
					</div>
				</td>
			</tr>
		</table>
	</div>

	
</body>
</html>